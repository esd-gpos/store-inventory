package mobile

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/esd-gpos/store-inventory/item"
	"gitlab.com/esd-gpos/store-inventory/store"
)

func RegisterMobileRoutes(storeHandler *store.Handler, itemHandler *item.Handler, engine *gin.Engine, auth *store.Middleware) {
	group := engine.Group("mobile/api/v1/store")
	group.Use(auth.TokenMiddleware)

	group.GET(":storeId", storeHandler.GetStore)

	group.GET("/:storeId/item", itemHandler.ListItem)
	group.PATCH("/:storeId/item/:itemId", itemHandler.UpdateItem)
}
