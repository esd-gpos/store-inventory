package item

import (
	"context"
	"encoding/json"
	"errors"
	"io/ioutil"
	"net/http"
	"strconv"
)

// Implementing ProdRepo interface
type HttpProdRepo struct {
	Endpoint string
}

func (h *HttpProdRepo) GetProduct(ctx context.Context, entity *Entity, authHeader string) error {
	request, err := http.NewRequest(http.MethodGet, h.Endpoint+entity.ProductId, nil)
	if err != nil {
		return err
	}

	request.Header.Set("Authorization", authHeader)

	// Call endpoint
	resp, err := http.DefaultClient.Do(request)
	if err != nil {
		return err
	}

	// Checks code
	if resp.StatusCode != http.StatusOK {
		if resp.StatusCode == http.StatusNotFound {
			return errProdNotFound
		}

		return errors.New(strconv.Itoa(resp.StatusCode))
	}

	// Unmarshal body
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return err
	}

	prodResult := &ProdResult{}
	if err = json.Unmarshal(body, prodResult); err != nil {
		return err
	}

	// Add information to Entity
	prod := prodResult.Result

	entity.Name = prod.Name
	entity.Tags = prod.Tags
	entity.Price = prod.Price
	entity.Desc = prod.Desc

	return nil
}

type ProdResult struct {
	Result Product `json:"result"`
}

type Product struct {
	Name  string   `json:"name"`
	Tags  []string `json:"tags"`
	Price float64  `json:"price"`
	Desc  string   `json:"desc"`
}
