package item

import (
	"context"
	"errors"
	"github.com/gin-gonic/gin"
	"github.com/google/uuid"
	"gitlab.com/esd-gpos/auth/middleware"
	"net/http"
)

func RegisterRoutes(handler *Handler, engine *gin.Engine, auth *middleware.Middleware) {
	group := engine.Group("api/v1/store/:storeId/item")
	group.Use(auth.GinHandler)

	group.POST("", handler.AddItem)
	group.GET("", handler.ListItem)
	group.PATCH(":itemId", handler.UpdateItem)
	group.DELETE(":itemId", handler.DeleteItem)
}

// Represent our Item to be added
type Entity struct {
	ItemId    string   `json:"item_id"`
	StoreId   string   `json:"-"`
	ProductId string   `json:"product_id"`
	UserId    string   `json:"-"`
	Name      string   `json:"name"`
	Tags      []string `json:"tags"`
	Price     float64  `json:"price"`
	Desc      string   `json:"desc"`
	Quantity  int      `json:"quantity"`
}

// Represent the object to be used for update
type UpdateEntity struct {
	ItemId   string
	StoreId  string
	UserId   string
	Quantity int
}

// AddItem represent the request body for AddItem
type AddItem struct {
	ProductId string `json:"product_id" binding:"required,uuid4"`
	Quantity  int    `json:"quantity" binding:"gte=0"`
}

// UpdateItem represent the request body for UpdateItem
type UpdateItem struct {
	Quantity int `json:"quantity" binding:"gte=0"`
}

// This repo interacts with the data source containing our inventory
var (
	errItemNotFound  = errors.New("item not found")
	errItemDuplicate = errors.New("item already exist")
	errStoreNotFound = errors.New("store not found")
)

type Repo interface {
	AddItem(ctx context.Context, entity *Entity) error

	ListItem(ctx context.Context, userId string, storeId string) ([]*Entity, error)

	UpdateItem(ctx context.Context, updateEntity *UpdateEntity) error

	AddItemQuantity(ctx context.Context, updateEntity *UpdateEntity) error

	SubtractItemQuantity(ctx context.Context, updateEntity *UpdateEntity) error

	DeleteItem(ctx context.Context, userId string, storeId string, itemId string) error
}

// This repo calls the product service to retrieve product information
var errProdNotFound = errors.New("product not found")

type ProductRepo interface {
	GetProduct(ctx context.Context, entity *Entity, authHeader string) error
}

// Handler for inventory endpoint
const (
	// Response messages for handlers
	resAddItem    = "added item to store inventory"
	resListItem   = "list of items retrieved"
	resUpdateItem = "item updated"
	resDeleteItem = "item deleted from store"

	// Error responses for handlers
	resInvalid       = "invalid format"
	resInternal      = "internal error"
	resProdNotFound  = "cannot find product"
	resItemNotFound  = "cannot find item"
	resStoreNotFound = "cannot find store"
	resConflict      = "item already added"
)

type Handler struct {
	Repo     Repo
	ProdRepo ProductRepo
}

func (h *Handler) AddItem(ctx *gin.Context) {
	userId := ctx.GetString("userId")

	storeId := ctx.Param("storeId")
	if _, err := uuid.Parse(storeId); err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{"message": resInvalid})
		return
	}

	body := &AddItem{}
	if err := ctx.ShouldBindJSON(body); err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{"message": resInvalid})
		return
	}

	// Retrieve product information first
	entity := &Entity{
		ItemId:    uuid.New().String(),
		StoreId:   storeId,
		ProductId: body.ProductId,
		UserId:    userId,
		Quantity:  body.Quantity,
	}

	if err := h.ProdRepo.GetProduct(ctx, entity, ctx.GetHeader("Authorization")); err != nil {
		if err == errProdNotFound {
			ctx.JSON(http.StatusNotFound, gin.H{"message": resProdNotFound, "code": err.Error()})
		} else {
			ctx.JSON(http.StatusInternalServerError, gin.H{"message": resInternal, "err": err.Error()})
		}

		return
	}

	// Add to data store
	if err := h.Repo.AddItem(ctx, entity); err != nil {
		if err == errItemDuplicate {
			ctx.JSON(http.StatusConflict, gin.H{"message": resConflict})
		} else if err == errStoreNotFound {
			ctx.JSON(http.StatusNotFound, gin.H{"message": resStoreNotFound})
		} else {
			ctx.JSON(http.StatusInternalServerError, gin.H{"message": resInternal})
		}

		return
	}

	ctx.JSON(http.StatusCreated, gin.H{"message": resAddItem})
}

func (h *Handler) ListItem(ctx *gin.Context) {
	userId := ctx.GetString("userId")

	storeId := ctx.Param("storeId")
	if _, err := uuid.Parse(storeId); err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{"message": resInvalid})
		return
	}

	entities, err := h.Repo.ListItem(ctx, userId, storeId)
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, gin.H{"message": resInternal})
		return
	}

	ctx.JSON(http.StatusOK, gin.H{"message": resListItem, "result": entities})
}

func (h *Handler) UpdateItem(ctx *gin.Context) {
	userId := ctx.GetString("userId")

	storeId := ctx.Param("storeId")
	if _, err := uuid.Parse(storeId); err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{"message": resInvalid})
		return
	}

	itemId := ctx.Param("itemId")
	if _, err := uuid.Parse(itemId); err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{"message": resInvalid})
		return
	}

	body := &UpdateItem{}
	if err := ctx.ShouldBindJSON(body); err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{"message": resInvalid})
		return
	}

	updateEntity := &UpdateEntity{
		ItemId:   itemId,
		StoreId:  storeId,
		UserId:   userId,
		Quantity: body.Quantity,
	}

	var err error
	if ctx.Query("add") != "" {
		err = h.Repo.AddItemQuantity(ctx, updateEntity)
	} else if ctx.Query("subtract") != "" {
		err = h.Repo.SubtractItemQuantity(ctx, updateEntity)
	} else {
		err = h.Repo.UpdateItem(ctx, updateEntity)
	}

	if err != nil {
		if err == errItemNotFound {
			ctx.JSON(http.StatusNotFound, gin.H{"message": resItemNotFound})
		} else {
			ctx.JSON(http.StatusInternalServerError, gin.H{"message": resInternal})
		}

		return
	}

	ctx.JSON(http.StatusOK, gin.H{"message": resUpdateItem, "result": updateEntity.Quantity})
}

func (h *Handler) DeleteItem(ctx *gin.Context) {
	userId := ctx.GetString("userId")

	storeId := ctx.Param("storeId")
	if _, err := uuid.Parse(storeId); err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{"message": resInvalid})
		return
	}

	itemId := ctx.Param("itemId")
	if _, err := uuid.Parse(itemId); err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{"message": resInvalid})
		return
	}

	if err := h.Repo.DeleteItem(ctx, userId, storeId, itemId); err != nil {
		if err == errItemNotFound {
			ctx.JSON(http.StatusNotFound, gin.H{"message": resItemNotFound})
		} else {
			ctx.JSON(http.StatusInternalServerError, gin.H{"message": resInternal})
		}

		return
	}

	ctx.JSON(http.StatusOK, gin.H{"message": resDeleteItem})
}
