package item

import (
	"context"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

// Implementing Repo interface
type MongoRepo struct {
	Col      *mongo.Collection
	StoreCol *mongo.Collection
}

func (m *MongoRepo) AddItem(ctx context.Context, entity *Entity) error {
	// Check if store exist
	count, err := m.StoreCol.CountDocuments(ctx, bson.M{"_id": entity.StoreId, "user_id": entity.UserId})
	if err != nil {
		return err
	}

	if count != 1 {
		return errStoreNotFound
	}

	// Do insert
	_, err = m.Col.InsertOne(ctx, bson.M{
		"_id":        entity.ItemId,
		"store_id":   entity.StoreId,
		"product_id": entity.ProductId,
		"user_id":    entity.UserId,
		"name":       entity.Name,
		"tags":       entity.Tags,
		"price":      entity.Price,
		"desc":       entity.Desc,
		"quantity":   entity.Quantity,
	})
	if err != nil {
		writeException, ok := err.(mongo.WriteException)
		if !ok {
			return err
		}

		writeError := writeException.WriteErrors

		// if not write error - some internal error
		if len(writeError) == 0 {
			return err
		}

		// if duplicate error
		if writeError[0].Code == 11000 {
			return errItemDuplicate
		}

		return err
	}

	return nil
}

func (m *MongoRepo) ListItem(ctx context.Context, userId string, storeId string) ([]*Entity, error) {
	cursor, err := m.Col.Find(ctx, bson.M{"user_id": userId, "store_id": storeId})
	if err != nil {
		return nil, err
	}

	entities := make([]*Entity, 0)

	for cursor.Next(ctx) {
		resultBody := &MongoResult{}
		if err = cursor.Decode(resultBody); err != nil {
			return nil, err
		}

		entities = append(entities, &Entity{
			ItemId:    resultBody.ItemId,
			StoreId:   resultBody.StoreId,
			ProductId: resultBody.ProductId,
			UserId:    resultBody.UserId,
			Name:      resultBody.Name,
			Tags:      resultBody.Tags,
			Price:     resultBody.Price,
			Desc:      resultBody.Desc,
			Quantity:  resultBody.Quantity,
		})
	}

	return entities, nil
}

func (m *MongoRepo) UpdateItem(ctx context.Context, updateEntity *UpdateEntity) error {
	projection := options.FindOneAndUpdate().SetProjection(bson.M{"_id": 1})

	result := m.Col.FindOneAndUpdate(ctx, bson.M{"_id": updateEntity.ItemId, "user_id": updateEntity.UserId, "store_id": updateEntity.StoreId},
		bson.M{"$set": bson.M{"quantity": updateEntity.Quantity}}, projection)

	if result.Err() != nil {
		if result.Err() == mongo.ErrNoDocuments {
			return errItemNotFound
		}

		return result.Err()
	}

	return nil
}

func (m *MongoRepo) AddItemQuantity(ctx context.Context, updateEntity *UpdateEntity) error {
	projection := options.FindOneAndUpdate().SetProjection(bson.M{"quantity": 1})

	result := m.Col.FindOneAndUpdate(ctx, bson.M{"_id": updateEntity.ItemId, "user_id": updateEntity.UserId, "store_id": updateEntity.StoreId},
		bson.M{"$inc": bson.M{"quantity": updateEntity.Quantity}}, projection)

	if result.Err() != nil {
		if result.Err() == mongo.ErrNoDocuments {
			return errItemNotFound
		}

		return result.Err()
	}

	temp := updateEntity.Quantity
	if err := result.Decode(updateEntity); err != nil {
		return err
	}

	updateEntity.Quantity += temp
	return nil
}

func (m *MongoRepo) SubtractItemQuantity(ctx context.Context, updateEntity *UpdateEntity) error {
	projection := options.FindOneAndUpdate().SetProjection(bson.M{"quantity": 1})

	result := m.Col.FindOneAndUpdate(ctx, bson.M{"_id": updateEntity.ItemId, "user_id": updateEntity.UserId, "store_id": updateEntity.StoreId},
		bson.M{"$inc": bson.M{"quantity": updateEntity.Quantity * -1}}, projection)

	if result.Err() != nil {
		if result.Err() == mongo.ErrNoDocuments {
			return errItemNotFound
		}

		return result.Err()
	}

	temp := updateEntity.Quantity
	if err := result.Decode(updateEntity); err != nil {
		return err
	}

	updateEntity.Quantity -= temp
	return nil
}

func (m *MongoRepo) DeleteItem(ctx context.Context, userId string, storeId string, itemId string) error {
	projection := options.FindOneAndDelete().SetProjection(bson.M{"_id": 1})

	result := m.Col.FindOneAndDelete(ctx, bson.M{"_id": itemId, "user_id": userId, "store_id": storeId}, projection)
	if result.Err() != nil {
		if result.Err() == mongo.ErrNoDocuments {
			return errItemNotFound
		}

		return result.Err()
	}

	return nil
}

type MongoResult struct {
	ItemId    string   `bson:"_id"`
	StoreId   string   `bson:"store_id"`
	ProductId string   `bson:"product_id"`
	UserId    string   `bson:"user_id"`
	Name      string   `json:"name"`
	Tags      []string `json:"tags"`
	Price     float64  `json:"price"`
	Desc      string   `json:"desc"`
	Quantity  int      `json:"quantity"`
}
