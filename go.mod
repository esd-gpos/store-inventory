module gitlab.com/esd-gpos/store-inventory

go 1.13

require (
	github.com/gin-contrib/cors v1.3.1 // indirect
	github.com/gin-gonic/gin v1.6.2
	github.com/go-playground/validator/v10 v10.2.0
	github.com/golang/protobuf v1.3.5 // indirect
	github.com/google/uuid v1.1.1
	github.com/joho/godotenv v1.3.0
	gitlab.com/esd-gpos/auth v0.0.0-20200330100020-24e8e06e3103 // indirect
	go.mongodb.org/mongo-driver v1.3.1
	golang.org/x/sys v0.0.0-20200331124033-c3d80250170d // indirect
)
