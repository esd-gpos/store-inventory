package store

import (
	"context"
	"errors"
	"github.com/gin-gonic/gin"
	"github.com/gin-gonic/gin/binding"
	"github.com/go-playground/validator/v10"
	"github.com/google/uuid"
	"gitlab.com/esd-gpos/auth/middleware"
	"net/http"
	"strings"
)

func RegisterRoutes(handler *Handler, engine *gin.Engine, auth *middleware.Middleware) {
	_ = addValidation()

	group := engine.Group("api/v1/store")
	group.Use(auth.GinHandler)

	group.POST("", handler.AddStore)
	group.GET("", handler.ListStore)
	group.GET(":storeId", handler.GetStore)
	group.PATCH(":storeId", handler.UpdateStore)
	group.DELETE(":storeId", handler.DeleteStore)
	group.POST("/:storeId/token", handler.GenerateToken)
}

// Represent a store object
type Entity struct {
	StoreId string `json:"store_id"`
	UserId  string `json:"-"`
	Name    string `json:"name"`
	Desc    string `json:"desc"`
	Token   string `json:"token"`
}

// AddStore represent the request body for AddStore handler
type AddStore struct {
	Name string `json:"name" binding:"store_name"`
	Desc string `json:"desc"`
}

// UpdateStore represent the request body for UpdateStore handler
type UpdateStore struct {
	Name string `json:"name" binding:"store_name"`
	Desc string `json:"desc"`
}

// Field level validation
func NameValidation(fl validator.FieldLevel) bool {
	if strings.TrimSpace(fl.Field().String()) == "" {
		return false
	}

	return true
}

// Add Validation to binding
func addValidation() error {
	validate := binding.Validator.Engine().(*validator.Validate)
	if err := validate.RegisterValidation("store_name", NameValidation); err != nil {
		return err
	}

	return nil
}

// Repo interface for interacting with data source
var (
	errNotFound  = errors.New("store not found")
	errDuplicate = errors.New("store with same name already exist")
)

type Repo interface {
	AddStore(ctx context.Context, entity *Entity) error

	ListStore(ctx context.Context, userId string) ([]*Entity, error)

	GetStore(ctx context.Context, entity *Entity) error

	UpdateStore(ctx context.Context, entity *Entity) error

	DeleteStore(ctx context.Context, userId string, storeId string) error

	GenerateToken(ctx context.Context, userId string, storeId string, token string) error
}

// Handler for Gin
const (
	// Success messages for handlers
	resAddStore      = "store added"
	resListStore     = "list of stores retrieved"
	resGetStore      = "store information retrieved"
	resUpdateStore   = "store updated"
	resDeleteStore   = "store deleted"
	resGenerateToken = "store token generated"

	// Errors messages for handlers
	resInvalid  = "invalid format"
	resInternal = "internal sever error"
	resNotFound = "cannot find store"
	resConflict = "store with that name already exist"
)

type Handler struct {
	Repo Repo
}

func (h *Handler) AddStore(ctx *gin.Context) {
	userId := ctx.GetString("userId")

	body := &AddStore{}
	if err := ctx.ShouldBindJSON(body); err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{"message": resInvalid})
		return
	}

	entity := &Entity{
		UserId:  userId,
		StoreId: uuid.New().String(),
		Name:    body.Name,
		Desc:    body.Desc,
		Token:   uuid.New().String(),
	}

	if err := h.Repo.AddStore(ctx, entity); err != nil {
		if err == errDuplicate {
			ctx.JSON(http.StatusConflict, gin.H{"message": resConflict})
		} else {
			ctx.JSON(http.StatusInternalServerError, gin.H{"message": resInternal, "err": err.Error(), "detail": err})
		}

		return
	}

	ctx.JSON(http.StatusCreated, gin.H{"message": resAddStore, "result": entity})
}

func (h *Handler) ListStore(ctx *gin.Context) {
	userId := ctx.GetString("userId")

	entities, err := h.Repo.ListStore(ctx, userId)
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, gin.H{"message": resInternal})
		return
	}

	ctx.JSON(http.StatusOK, gin.H{"message": resListStore, "result": entities})
}

func (h *Handler) GetStore(ctx *gin.Context) {
	userId := ctx.GetString("userId")

	storeId := ctx.Param("storeId")
	if _, err := uuid.Parse(storeId); err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{"message": resInvalid})
		return
	}

	entity := &Entity{
		StoreId: storeId,
		UserId:  userId,
	}

	if err := h.Repo.GetStore(ctx, entity); err != nil {
		if err == errNotFound {
			ctx.JSON(http.StatusNotFound, gin.H{"message": resNotFound})
		} else {
			ctx.JSON(http.StatusInternalServerError, gin.H{"message": resInternal, "err": err.Error(), "detail": err})
		}

		return
	}

	ctx.JSON(http.StatusOK, gin.H{"message": resGetStore, "result": entity})
}

func (h *Handler) UpdateStore(ctx *gin.Context) {
	userId := ctx.GetString("userId")

	storeId := ctx.Param("storeId")
	if _, err := uuid.Parse(storeId); err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{"message": resInvalid})
		return
	}

	body := &UpdateStore{}
	if err := ctx.ShouldBindJSON(&body); err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{"message": resInvalid})
		return
	}

	entity := &Entity{
		UserId:  userId,
		StoreId: storeId,
		Name:    body.Name,
		Desc:    body.Desc,
		Token:   "",
	}

	if err := h.Repo.UpdateStore(ctx, entity); err != nil {
		if err == errNotFound {
			ctx.JSON(http.StatusNotFound, gin.H{"message": resNotFound})
		} else if err == errDuplicate {
			ctx.JSON(http.StatusConflict, gin.H{"message": resConflict})
		} else {
			ctx.JSON(http.StatusInternalServerError, gin.H{"message": resInternal, "err": err.Error(), "detail": err})
		}

		return
	}

	ctx.JSON(http.StatusOK, gin.H{"message": resUpdateStore, "result": entity})
}

func (h *Handler) DeleteStore(ctx *gin.Context) {
	userId := ctx.GetString("userId")

	storeId := ctx.Param("storeId")
	if _, err := uuid.Parse(storeId); err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{"message": resInvalid})
		return
	}

	if err := h.Repo.DeleteStore(ctx, userId, storeId); err != nil {
		if err == errNotFound {
			ctx.JSON(http.StatusNotFound, gin.H{"message": resNotFound})
		} else {
			ctx.JSON(http.StatusInternalServerError, gin.H{"message": resInternal, "err": err.Error(), "detail": err})
		}

		return
	}

	ctx.JSON(http.StatusOK, gin.H{"message": resDeleteStore})
}

func (h *Handler) GenerateToken(ctx *gin.Context) {
	userId := ctx.GetString("userId")

	storeId := ctx.Param("storeId")
	if _, err := uuid.Parse(storeId); err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{"message": resInvalid})
		return
	}

	token := uuid.New().String()
	if err := h.Repo.GenerateToken(ctx, userId, storeId, token); err != nil {
		if err == errNotFound {
			ctx.JSON(http.StatusNotFound, gin.H{"message": resNotFound})
		} else {
			ctx.JSON(http.StatusInternalServerError, gin.H{"message": resInternal})
		}

		return
	}

	ctx.JSON(http.StatusCreated, gin.H{"message": resGenerateToken, "result": token})
}
