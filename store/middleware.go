package store

import (
	"context"
	"errors"
	"github.com/gin-gonic/gin"
	"github.com/google/uuid"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"net/http"
)

// Below contains the middleware the will retrieve store information
// Entity used by the middleware
type MiddlewareEntity struct {
	UserId string `bson:"user_id"`
	Token  string `bson:"token"`
}

// Middleware Repo for interacting with data source
var errTokenNotFound = errors.New("token not found")

type MiddlewareRepo interface {
	GetStoreInformation(ctx context.Context, middleEntity *MiddlewareEntity) error
}

// Middleware that retrieve store token and set appropiate context
type Middleware struct {
	Repo MiddlewareRepo
}

func (m *Middleware) TokenMiddleware(ctx *gin.Context) {
	token := ctx.GetHeader("store-token")
	if _, err := uuid.Parse(token); err != nil {
		ctx.AbortWithStatusJSON(http.StatusUnauthorized, gin.H{"message": "missing store token"})
		return
	}

	middleEntity := &MiddlewareEntity{Token: token}
	if err := m.Repo.GetStoreInformation(ctx, middleEntity); err != nil {
		if err == errTokenNotFound {
			ctx.AbortWithStatusJSON(http.StatusUnauthorized, gin.H{"message": "token does not match"})
		} else {
			ctx.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"message": "not your fault"})
		}

		return
	}

	ctx.Set("userId", middleEntity.UserId)
}

// Implementation of MiddlewareRepo
type MiddlewareMongoRepo struct {
	Col *mongo.Collection
}

func (m *MiddlewareMongoRepo) GetStoreInformation(ctx context.Context, middleEntity *MiddlewareEntity) error {
	res := m.Col.FindOne(ctx, bson.M{"token": middleEntity.Token})
	if res.Err() != nil {
		if res.Err() == mongo.ErrNoDocuments {
			return errTokenNotFound
		}

		return res.Err()
	}

	if err := res.Decode(middleEntity); err != nil {
		return err
	}

	return nil
}
