package store

import (
	"context"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

type MongoRepo struct {
	Col *mongo.Collection
}

func (m *MongoRepo) AddStore(ctx context.Context, entity *Entity) error {
	_, err := m.Col.InsertOne(ctx, bson.M{
		"_id":     entity.StoreId,
		"user_id": entity.UserId,
		"name":    entity.Name,
		"desc":    entity.Desc,
		"token":   entity.Token,
	})
	if err != nil {
		writeException, ok := err.(mongo.WriteException)
		if !ok {
			return err
		}

		writeError := writeException.WriteErrors

		// if not write error - some internal error
		if len(writeError) == 0 {
			return err
		}

		// if duplicate error
		if writeError[0].Code == 11000 {
			return errDuplicate
		}

		return err
	}

	return nil
}

func (m *MongoRepo) ListStore(ctx context.Context, userId string) ([]*Entity, error) {
	cursor, err := m.Col.Find(ctx, bson.M{"user_id": userId})
	if err != nil {
		return nil, err
	}

	entities := make([]*Entity, 0)
	for cursor.Next(ctx) {
		resultBody := &MongoResult{}

		if err = cursor.Decode(resultBody); err != nil {
			return nil, err
		}

		entities = append(entities, &Entity{
			UserId:  resultBody.UserId,
			StoreId: resultBody.StoreId,
			Name:    resultBody.Name,
			Desc:    resultBody.Desc,
			Token:   resultBody.Token,
		})
	}

	return entities, nil
}

func (m *MongoRepo) GetStore(ctx context.Context, entity *Entity) error {
	result := m.Col.FindOne(ctx, bson.M{"_id": entity.StoreId, "user_id": entity.UserId})
	if result.Err() != nil {
		if result.Err() == mongo.ErrNoDocuments {
			return errNotFound
		}

		return result.Err()
	}

	resultBody := &MongoResult{}
	if err := result.Decode(resultBody); err != nil {
		return err
	}

	entity.Name = resultBody.Name
	entity.Desc = resultBody.Desc
	entity.Token = resultBody.Token

	return nil
}

func (m *MongoRepo) UpdateStore(ctx context.Context, entity *Entity) error {
	result := m.Col.FindOneAndUpdate(ctx, bson.M{"_id": entity.StoreId, "user_id": entity.UserId}, bson.M{
		"$set": bson.M{"name": entity.Name, "desc": entity.Desc},
	})

	if result.Err() != nil {
		if result.Err() == mongo.ErrNoDocuments {
			return errNotFound
		}

		if err, ok := result.Err().(mongo.CommandError); ok {
			if err.Code == 11000 {
				return errDuplicate
			}
		}

		return result.Err()
	}

	resultBody := &MongoResult{}
	if err := result.Decode(resultBody); err != nil {
		return err
	}

	entity.Token = resultBody.Token

	return nil
}

func (m *MongoRepo) DeleteStore(ctx context.Context, userId string, storeId string) error {
	projection := options.FindOneAndDelete().SetProjection(bson.M{"_id": 1, "user_id": 1})

	result := m.Col.FindOneAndDelete(ctx, bson.M{"_id": storeId, "user_id": userId}, projection)
	if result.Err() != nil {
		if result.Err() == mongo.ErrNoDocuments {
			return errNotFound
		}

		return result.Err()
	}

	return nil
}

func (m *MongoRepo) GenerateToken(ctx context.Context, userId string, storeId string, token string) error {
	projection := options.FindOneAndUpdate().SetProjection(bson.M{"_id": 1, "user_id": 1})

	result := m.Col.FindOneAndUpdate(ctx, bson.M{"_id": storeId, "user_id": userId}, bson.M{
		"$set": bson.M{"token": token},
	}, projection)

	if result.Err() != nil {
		if result.Err() == mongo.ErrNoDocuments {
			return errNotFound
		}

		return result.Err()
	}

	return nil
}

type MongoResult struct {
	StoreId string `bson:"_id"`
	UserId  string `json:"user_id"`
	Name    string `json:"name"`
	Desc    string `json:"desc"`
	Token   string `json:"token"`
}
